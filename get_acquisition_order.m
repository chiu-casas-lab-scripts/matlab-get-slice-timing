function [TR, slice_times, nslices, TA, ...
    slice_order, reference_slice] = get_acquisition_sequence(functional_dicom_filename)
d = dicominfo(functional_dicom_filename);

if isfield(d, 'SoftwareVersion')
    software_version = d.SoftwareVersion;
elseif isfield(d, 'SoftwareVersions')
    software_version = d.SoftwareVersions;
else
    error('Could not identify software version of dicom file.');
end

if contains(software_version, 'XA30')
    [TR, slice_times, nslices, TA, ...
        slice_order, reference_slice] = xa30_get_acquisition_order(d);
else
    [TR, slice_times, nslices, TA, ...
        slice_order, reference_slice] = trio_get_acquisition_order(d);
end
end


function [TR, slice_times, nslices, TA, ...
    slice_order, reference_slice] = trio_get_acquisition_order(hdr)
TR = round(hdr.RepetitionTime / 1000);
slice_times = hdr.Private_0019_1029;
nslices = length(slice_times);
TA = TR - (TR / nslices);
[~, slice_order] = sort(slice_times);
reference_slice = slice_order(round(nslices/2));
end

function [TR, slice_times, nslices, TA, ...
    slice_order, reference_slice] = xa30_get_acquisition_order(hdr)
repetition_time = hdr.SharedFunctionalGroupsSequence.Item_1.MRTimingAndRelatedParametersSequence.Item_1.RepetitionTime;
TR = round(repetition_time / 1000);
nslices = hdr.NumberOfFrames;
TA = TR - (TR / nslices);
struct_fields = cell(nslices, 1);
% Create a list of structure fields for each slice
for ii = 1 : nslices
    struct_fields{ii} = sprintf('Item_%d', ii);
end
slice_times = nan(nslices, 1);
for ii = 1 : length(struct_fields)
    aqt = hdr.PerFrameFunctionalGroupsSequence.(struct_fields{ii}).FrameContentSequence.Item_1.FrameAcquisitionDateTime;
    slice_times(ii) = str2num(aqt);
end
[~, slice_order] = sort(slice_times);
reference_slice = slice_order(round(nslices/2));
end